<?php

class Building {
    
    // Properties
    private $name;
    private $floors;
    private $address;

    // Constructor
    public function __construct($name, $floors, $address) {
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }

    // getters and setters function
    public function getName() {
        return $this->name;
    }

    public function getFloors() {
        return $this->floors;
    }

    public function getAddress() {
        return $this->address;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setFloors($floors) {
        $this->floors = $floors;
    }

    public function setAddress($address) {
        $this->address = $address;
    }
}

class Condominium extends Building {


}

$building = new Building('Caswyn Building', 8, 'Timog Avenue Quezon City, Philippines');
$condominium = new Condominium('Enzo Building', 12, 'Manila City, Philippines');

class Animal {

    //Properties
    public $name;
    public $family;
    public $gender;
    public $color;
    public $action;

    // Contructor 
    public function __construct($name, $family, $gender, $color, $action) {
        $this->name = $name;
        $this->family = $family;
        $this->gender = $gender;
        $this->color = $color;
        $this->action = $action;
    }

    // getters and setters function
    public function getName() {
        return $this->name;
    }

    public function getFamily() {
        return $this->family;
    }

    public function getGender() {
        return $this->gender;
    }

    public function getColor() {
        return $this->color;
    }

    public function getAction() {
        return $this->action;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setFamily($family) {
        $this->family = $family;
    }

    public function setColor($color) {
        $this->color = $color;
    }

    public function setAction($action) {
        $this->action = $action;
    }

    // Methods
    public function printAnimal() {
        return "Name: $this->name <br>
        Gender: $this->gender <br>
        Color: $this->color <br>
        Family: $this->family <br>";
    }

    public function printAnimalAction() {
        return "$this->name is $this->action.";
    }

}

class Dog extends Animal {}
class Cat extends Animal {}

$dog = new Animal('Chichi', 'Poodle', 'female', 'white', 'eating');
$cat = new Animal('Cardo', 'Domestic short-haired', 'male', 'gray', 'sleeping');

