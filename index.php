<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S04: ACTIVITY for Access Modifiers and Encapsulation</title>
</head>
<body>

    <h1>Activity Solution</h1>

    <h2>Building</h2>
    <p>The name of the building is <?= $building->getName(); ?>.</p>
    <p>The <?= $building->getName(); ?> has <?= $building->getFloors(); ?> floors.</p>
    <p>The <?= $building->getName(); ?> is located at <?= $building->getAddress(); ?>.</p>
    <?php $building->setName('Caswyn Complex'); ?>
    <p>The name of the building has been changed to <?= $building->getName(); ?>. </p>

    <h2>Condominium</h2>
    <p>The name of the condominium is <?= $condominium->getName(); ?>.</p>
    <p>The <?= $condominium->getName(); ?> has <?= $condominium->getFloors(); ?> floors.</p>
    <p>The <?= $condominium->getName(); ?> is located at <?= $condominium->getAddress(); ?>.</p>
    <?php $condominium->setName('Enzo Tower'); ?>
    <p>The name of the condominium has been changed to <?= $condominium->getName(); ?>. </p>

    <h2>Animals</h2>

    <h3>Dog</h3>
    <p>The name of the dog is <?= $dog->getName(); ?>.</p>
    <p><?= $dog->printAnimal(); ?></p>
    <p><?= $dog->getAction(); ?></p>

    <h3>Cat</h3>
    <p>The name of the cat is <?= $cat->getName(); ?>.</p>
    <p><?= $cat->printAnimal(); ?></p>
    <p><?= $cat->getAction(); ?></p>

</body>
</html>